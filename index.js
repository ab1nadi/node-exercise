// Abinadi Swapp
// 12/11/2022
// An express server that consumes the SWAPI (Star Wars API)
// and serves the people and planets SWAPI routes on its own
// endpoints. 
// It uses axios and lodash for API consumption.
// It uses lodash for sorting and cleaning of object collections.


const SWAPI_PEOPLE = "https://swapi.dev/api/people/";            // the swapi api url for people
const SWAPI_PLANETS = "https://swapi.dev/api/planets/";          // the swappi api url for planets

const SORTABLE_FIELDS = ['name', 'height', 'mass'];              // the fields we want to be able to sort by

const axios = require('axios');                 // used for api calls
const _ = require('lodash');                    // used to sort object arrays 
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = process.env.PORT || 4000;
const app = express();

app.use(cors())

// settup the middleware for extracting query params
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// the /people endpoint
app.get('/people', (req, res) => people(req,res));

// the /planets endpoint 
app.get('/planets', (req, res) => planets(req,res));

app.listen(PORT, () => console.log(`listening on port ${PORT}!`));



// people
// called by the people endpoint
 function people(req,res)
{
    let sortBy = req.query.sortBy;

    // get the people
    getPeople().then( people => 
    {
        // if they gave the sortby query
        // and its included in sortable_fields
        if(sortBy && SORTABLE_FIELDS.includes(sortBy))
        {
            // cleans the data so that "Jabba Desilijic Tiure" with mass=1,358 will actually be 1358 when sorting
            // and does other things like convert numbers to floats for comparison if applicable
            people = _.forEach(people, (o)=> clean(o,sortBy));

            // does the ordering
            people = _.sortBy(people, sortBy, 'asc');

            // restores the old value from what was stored in the object
            // but it is now sorted
            people = _.forEach(people, (o)=>restoreOld(o, sortBy));
        }

        // send the data 
        res.send(people);
    }).catch( err => 
        {
            res.status(500).send(err);
        })
    
}

// planets 
//
function planets(req,res)
{
        // get the people
        getPlanets().then( planets => 
            {
                // send the data 
                res.send(planets);

            }).catch( err => 
                {
                    res.status(500).send(err);
                })
            
}

// getPeople
// consumes the Swapi api 
// returns a promise that it will
// get all people
function getPeople()
{
// return a new promise 
return new Promise(
     function (resolve, reject)
    {
        // We want everything to run async
        setTimeout( async function ( ) {

                // continuously add data to this array 
                let returned_data = [];

                let next = SWAPI_PEOPLE

                while(next)
                {
                    // synchronously get the data 
                    let response = await axios.get(next).then().catch(err =>reject(err))

                    // update next to the next page in the api 
                    next = response.data.next;

                    // get the data from this page 
                    returned_data = returned_data.concat(response.data.results);
                }

                resolve(returned_data);
            },0);
    }
);
}

// getPlanets
// consumes the swapi api
// returns a promise that it will 
// get all planets
function getPlanets()
{
    // return a new promise 
    return new Promise(
        async function (resolve, reject)
        {
            // We want everything to run async
            setTimeout( async function ( ) {

                    // continuously add data to this array 
                    let returned_data = [];

                    let next = SWAPI_PLANETS

                    while(next)
                    {
                        // synchronously get the data 
                        let response = await axios.get(next).then().catch(error =>reject(err))

                        // update next to the next page in the api 
                        next = response.data.next;

                        // get the names of residents 
                        // and replace them with actual names not urls 
                        _.forEach(response.data.results, (planet)=>
                        {
                            _.forEach(planet.residents, async function (resident, index)
                            {
                                // get the response
                                let resp = (await axios.get(resident).then().catch(err=>reject(err)));

                                // extract the name from the object into the resident
                                planet.residents[index]= resp.data.name;
                            });
                        })

                        // get the data from this page 
                        returned_data = returned_data.concat(response.data.results);
                    }

                    resolve(returned_data);
            },0);
        }
    );
}


// clean
// removes comma from a given property
// in a given object and saves the old
// string as an "old" property
function clean(object, property)
{
    // save the old data 
    object.old = object[property];

    // remove the comma if there is one 
    object[property] = object[property].replaceAll(',', '').trim();

    // if it is possible to turn it into a float do it
    // if it is unknow lets let it be NaN so they sort to the end no matter what (at least when we are sorting only numbers)
    if(!isNaN(parseFloat(object[property])) || object[property] == "unknown")
        object[property] = parseFloat(object[property]);

}


// restoreOld
// restores the "old" property 
// of a given object to a given 
// property in that object
// deletes the old property
function restoreOld(Object, property)
{
    Object[property] = Object.old;
    delete Object.old;
}